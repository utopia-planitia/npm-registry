apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/auth-realm: Authentication Required
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-type: basic
  name: npm-registry
spec:
  rules:
    - host: "{{ .Values.cluster_domain }}"
      http:
        paths:
          - backend:
              service:
                name: npm-registry
                port:
                  number: 80
            path: /
            pathType: ImplementationSpecific
  tls:
    - hosts:
        - "{{ .Values.cluster_domain }}"
      secretName: "{{ .Values.cluster_tls_wildcard_secret }}"
